package objecttracker;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.TermCriteria;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;
import org.opencv.videoio.VideoCapture;

public class FrameProcessor {
	JLabel videoScreen;
	JLabel backProjScreen;
	Mat frame;
	VideoCapture camera;
	boolean stop;
	List<Point> points = new ArrayList<>();
	
	public FrameProcessor(JLabel screen, JLabel backProjScreen) {
		this.videoScreen = screen;
		frame = new Mat();
		camera = new VideoCapture();
		this.backProjScreen = backProjScreen;
	}
	
	public void openWebCam() {
		camera.open(0);
	}
	
	public void openVideo(String path) {
		camera.open(path);
	}
	
	public void play() {
		Rect trackWindow = null;
		Rect rectCrop = null;
		Mat roi_hist = null;
		
		if(!points.isEmpty()){
			OptionalInt minX = points.stream().mapToInt(a -> (int)a.x).min();
			OptionalInt minY = points.stream().mapToInt(a -> (int)a.y).min();
			OptionalInt maxX = points.stream().mapToInt(a-> (int)a.x).max();
			OptionalInt maxY = points.stream().mapToInt(a -> (int)a.y).max();
		
			Point p1 = new Point(minX.getAsInt(), minY.getAsInt());
			Point p2 = new Point(maxX.getAsInt(), maxY.getAsInt());
		
			trackWindow = new Rect(p1, p2);
			rectCrop = new Rect(p1,p2);
		
			camera.read(frame);
			
			// podru�je interesa roi
			Mat roi= frame.submat(rectCrop);
			
			// konvertiranje podru�ja interesa u HSV oblik (hue, saturation, value)
			Mat hsv_roi = frame.submat(rectCrop);
			Imgproc.cvtColor(roi, hsv_roi, Imgproc.COLOR_BGR2HSV);
			
			
			Mat mask = frame.submat(rectCrop);
			Core.inRange(hsv_roi, new Scalar(0.,60.,32.), new Scalar(180., 255., 255.), mask);
			List<Mat> images = new ArrayList<Mat>();
			images.add(hsv_roi);
			roi_hist = frame.submat(rectCrop);
			Imgproc.calcHist(images, new MatOfInt(0), mask, roi_hist, new MatOfInt(180), new MatOfFloat(0,180));
			Core.normalize(roi_hist,roi_hist, 0, 255, Core.NORM_MINMAX);
			
		}
		
		stop = false;
		while (!stop && (!Thread.currentThread().isInterrupted())) {
			if (camera.read(frame)) {
				if(trackWindow != null) {
					// pretvorba trenutne slike u HSV oblik
					Mat hsv = new Mat();
					Imgproc.cvtColor(frame, hsv, Imgproc.COLOR_BGR2HSV);
					
					// priprema podataka za izra�un back projectiona
					Mat backProj = new Mat();
					List<Mat> images = new ArrayList<Mat>();
					images = new ArrayList<>();
					images.add(hsv);
					
					// izra�un Back projection
					Imgproc.calcBackProject(images, new MatOfInt(0), roi_hist, backProj, new MatOfFloat(0,180), 1);
					
					// pode�avanje kriterija terminiranja algoritma camShift
					TermCriteria termCriteria = new TermCriteria();
					termCriteria.type = TermCriteria.EPS | TermCriteria.COUNT;
					termCriteria.maxCount = 10;
					termCriteria.epsilon = 1;
					
					// izvr�avanje algoritma CAMSHIFT 
					RotatedRect ret = Video.CamShift(backProj, trackWindow, termCriteria);
					
					// iscrtavanje pravokutnika oko pra�enog objekta
					Point vertices[] = new Point[4];
				    ret.points(vertices);
				    for (int i = 0; i < 4; i++)
				        Imgproc.line(frame, vertices[i], vertices[(i+1)%4], new Scalar(0,255,0));
				    
				    ImageIcon image = new ImageIcon(Mat2BufferedImage(backProj));
					backProjScreen.getGraphics().drawImage(image.getImage(), 0, 0, null);
					reproduceFrame();
					
				} else {
			    // iscrtavanje slike na GUI
				reproduceFrame();
				
			}
				
			} else {
				String message = "Choose source!";
				videoScreen.setText(message);
				
			}
		}
		
		
		
	}
	
	private void reproduceFrame() {
		// iscrtavanje slike na GUI
		ImageIcon image = new ImageIcon(Mat2BufferedImage(frame));
		videoScreen.getGraphics().drawImage(image.getImage(), 0, 0, null);
	}
	
	public void stop(){
		stop = true;
		points.clear();
	}
	
	
	
	
	public static BufferedImage Mat2BufferedImage(Mat m) {
		// source:
		// http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/
		// Fastest code
		// The output can be assigned either to a BufferedImage or to an Image

		int type = BufferedImage.TYPE_BYTE_GRAY;
		if (m.channels() > 1) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		int bufferSize = m.channels() * m.cols() * m.rows();
		byte[] b = new byte[bufferSize];
		m.get(0, 0, b); // get all the pixels
		BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);
		return image;
	}
	
	public boolean isStop() {
		return stop;
	}
}
